<?php

    session_start();

    if(isset($_SESSION["connecte"]))
    {
        unset($_SESSION["connecte"]);
        unset($_SESSION["identidiant"]);
    }

    header("Location: ./login.php");
?>