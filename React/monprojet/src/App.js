import { Component, useEffect, useState } from "react";
import "./App.css";
import React from "react";

const Administration = ({ nom, description, contact, adresse }) => {
  return (
    <div>
      <div>
        <h3>{nom}</h3>
      </div>
      <div>{contact}</div>
      <div>{adresse}</div>
    </div>
  );
};

function App() {
  const [isClicked, setIsClicked] = useState(false);
  const [Items, setItems] = useState([]);

  useEffect(() => {
    if (isClicked) {
      var depts = document.getElementById("select-department").value;
      var adminiType = document.getElementById("select-administration-type").value;
      var url = "https://etablissements-publics.api.gouv.fr/v3/departements/"+ depts + "/" + adminiType ;
      fetch(url)
        .then((result) => result.json())
        .then((json) => setItems(json.features));
    }
    setIsClicked(false);
  }, [isClicked]);

  useEffect(() => {
    console.log("test")
    var div = document.getElementById("researchResult");
    div.innerHTML = ""
    if (Items.lenght != 0){
      for (let i = 0; i < Items.length; i++){
        console.log(Items[i]);
        var card = document.createElement('div');
        card.setAttribute("id", Items[i].properties.id);
        card.setAttribute("style", "border : 1px solid black")
        div.appendChild(card);
        document.getElementById(Items[i].properties.id).innerHTML = Items[i].properties.nom;
      }
    }
    setIsClicked(false);
  }, [Items])

  return (
    <div className="App">
      <div>
        <h1>Trouver son administration en Normandie</h1>
        <select id="select-department">
          <option value="14">Calvados</option>
          <option value="27">Eure</option>
          <option value="50">Manche</option>
          <option value="61">Orne</option>
          <option value="76">Seine Maritine</option>
        </select>
        <select id="select-administration-type">
          <option value="ars">Agence Regionale de Santé</option>
          <option value="cpam">Caisse Primaire d'Assurance Maladie</option>
        </select>
        <button name="search-administration" onClick={() => setIsClicked(true)}>
          Rechercher une administration
        </button>
        <button name="clear-research" onClick={() => setItems([])}>Vider la recherche</button>
      </div>
      <div id="researchResult">
        
      </div>
    </div>
  );
}

export default App;

// const Fruit = ({type,poid,children}) => {
//   return (
//     <div>Je suis un Fruit de type {type} et je pèse {poid}. {children} </div>
//   )
// }

// class App extends Component {
//   state = {
//     fruits: [{
//       type:"tomate",
//       poid:"69"
//     },{
//       type:"Fraise",
//       poid:"15"
//     }]
//   }

//   changeFruits = () => {
//     this.setState({
//       fruits: [{
//         type:"Fraise",
//         poid:"69"
//       },{
//         type:"Tomate",
//         poid:"15"
//       }]}
//     )

//   }

//   render() {
//     return (
//       <div className="App">
//       <h1>Mon project React</h1>
//       <Fruit type="pomme" poid="66g"> Je suis rouge.</Fruit>
//       <Fruit type="poire" poid="55g"/>
//       <Fruit type={this.state.fruits[0].type} poid={this.state.fruits[0].poid} />
//       <button onClick={this.changeFruits}>Changer les fruits</button>
//     </div>
//     );
//   }
// }

// function App() {
//   return (
//     <div className="App">
//       <h1>Mon project React</h1>
//       <Fruit type="pomme" poid="66g"> Je suis rouge.</Fruit>
//       <Fruit type="poire" poid="55g"/>
//     </div>
//   );
// }
