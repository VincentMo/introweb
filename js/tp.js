function supToTen(vari) {
    if(vari>=10) {
        document.write(vari + " est supérieur à 10 </br>")
    } else {
        document.write(vari + " est inférieur ou égal à 10 </br>")
    }
}

function affiche1to10() {
    for(i = 1 ; i<=10; i++){
        document.write(i + "</br>")
    }
}

function tabCoord() {
    document.write('<table border="1">');
    for (var temp1 = 1; temp1<=10 ;temp1++){
        document.write("<tr>")
        for (var temp2 = 1; temp2<=10 ;temp2++){
            document.write("<td>"+ temp1 +";" +temp2 +"</td>")
        }
        document.write("</tr>")
    }
    document.write('</table>')
}

function copie(idcible,idorigin) {
    document.getElementById(idcible).value = document.getElementById(idorigin).value
}

function checkQCM() {
    repQ1 = document.getElementById("Q1").value;
    repQ2 = document.getElementById("Q2").value;
    repQ3 = document.getElementById("Q3").value;
    var res = 0;
    if (repQ1 == "Yes") {
        res += 1;
    }
    if (repQ2 == "Blanc") {
        res += 1;
    }
    if (repQ3 == "Boire") {
        res += 1;
    }

    var x = document.getElementById("result");
    x.innerHTML = " Votre score au QCM est " + res +"/3";
}

function closePage(page) {
    windows.close()
    //! WARNING : must be a page opened by the script
}

function openPage() {
    //window.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be&ab_channel=RickAstleyVEVO")
}

function checkEmail() {
    var mail = document.getElementById("mailIn").value;
    const reg = /^([a-zA-Z0-9-.]+)@([a-zA-Z0-9-.]+).([a-zA-Z]{2,5})$/;

    if (!reg.test(mail)) {
        var x = document.getElementById("resEmail");
        x.innerHTML = "Le format de votre adresse email est faux"
    } else {
        var x = document.getElementById("resEmail");
        x.innerHTML = "Le format de votre adresse email est bon"
    }
}

function checkPhoneNumber() {
    var phoneNumber = document.getElementById("phoneNumberIn").value;
    const reg = /^((0([1-9])\.)(([0-9]{2})\.){3})([0-9]{2})$/;

    if (!reg.test(phoneNumber)) {
        var x = document.getElementById("resPhoneNumber");
        x.innerHTML = "Le format de votre numéro de téléphone est faux"
    } else {
        var x = document.getElementById("resPhoneNumber");
        x.innerHTML = "Le format de votre numéro de téléphone est bon"
    }
}

function checkLoginPasswd() {
    var login = document.getElementById("loginIn").value;
    var passwd = document.getElementById("passwdIn").value;
    var listLogin = ["Pierro","Valou","Momo"];
    var listPasswd = ["azerty","qsdfg","wxcvbn"];
    var temp3 = 0;
    var x = document.getElementById("resLoginPasswd");
    var resChaine =" Identifiant non valide";
    for (loginu in listLogin) {
        resChaine += login
    }
    /*
    if ( login in listLogin ){
        var place = -1
        for (temp = 0 ; temp < listLogin.length(); temp++) {
            if (login == listLogin[temp]){
                place = temp
            }
        }
        if (passwd == listPasswd[place] ){
            resChaine = "Identifiants correct"
        } else {
            resChaine = "Mot de passe incorrect"
        }
    } else {
        resChaine = "Identifiants incorrect"
    }*/
    /*for (var uLogin in listLogin) {
        if (login == uLogin) {
            resChaine = "Login corect"
            if (passwd == listPasswd[temp3]) {
                resChaine = "Identifiants correct"
            }else {
                resChaine += "/ Mot de passe incorrect"
            }
        } else {
            temp3 += 1;
        }
    }*/
    x.innerHTML = resChaine;
}