<?php

session_start();

if (!isset($_SESSION["connecte"]) && $_SESSION["connecte"] !== true) {
    header("Location: ./login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste</title>
</head>

<body>
    <a href="./deconnexion.php"><button>Deconnexion</button></a>
    <div>
        <p>Bienvenue <?= $_SESSION["identidiant"] ?></p>
    </div>

</body>

</html>